package com.sample.reactiveformsample

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.annotation.IntegerRes
import android.util.Log
import com.jakewharton.rxbinding.widget.RxTextView
import kotlinx.android.synthetic.main.activity_main.*
import rx.functions.Func1


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


//        RxTextView.textChanges(textinputan)
//                .map { charSequence ->Integer.parseInt(StringBuilder(charSequence).reverse().toString())}
//                .subscribe { value -> text_output.text ="sdfdsf " + value}

        RxTextView.textChanges(textinputan)
                .map(object :Func1<CharSequence, String>{
                    override fun call(t: CharSequence?): String {
                        val dataString = StringBuilder(t).toString()
                        if(dataString.isEmpty()) {
                            return "0"
                        }
                        return dataString
                    }
                }).map(object :Func1<String, Int>{
                    override fun call(t: String?): Int {
                        return Integer.parseInt(t)
                    }
                }).subscribe { value -> text_output.setText("sdfdsfds " + value) }
    }

}
